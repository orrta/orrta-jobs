help:
	@echo 'Makefile for orrta-jobs                                                        '
	@echo '                                                                                   '
	@echo 'Usage:                                                                             '
	@echo '   install                                    Install all dependencies to run      '
	@echo '   setup                                      Install all dependencies to dev      '
	@echo '   run                                        Run project                          '
	@echo '   test_code_quality                          Run project test code quality        '


install:
	@pip install -r requirements.txt

setup:
	@pip install --upgrade pip
	@pip install -r requirements_dev.txt

run:
	DEBUG=True \
	honcho start; true

test_code_quality:
	@echo "========================== TEST CODE QUALITY =========================="
	@py.test orrta_jobs --pep8 --flakes --mccabe

.PHONY: help, install, setup, run, test_code_quality
