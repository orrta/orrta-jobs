from orrta_jobs.redis import CacheProvider
from orrta_jobs.fcm import FcmNotifier

import json
import logging
import requests
import schedule
import time

from requests.auth import HTTPBasicAuth

logger = logging.getLogger('user_hearts_job')

es_host = 'https://5618aac2b24848f8877c7a43ee1340c9.us-east-1.aws.found.io:9243'
user = 'elastic'
password = 'u5Gcy1Q12saAZqP8A1tcBO1k'

batch_size = 32
search_url = '{es_host}/user/_search'.format(es_host=es_host)

query = {
    "size": batch_size,
    "query": {
        "term": {
            "subscriber": {
                "value": True
            }
        }
    }
}

cache_provider = CacheProvider()
fcmNotifier = FcmNotifier()


class UserHeartsJob():

    def clear_cache(self, ids, retries=0):
        try:
            logger.info('Cleaning cache')
            for id in ids:
                cache_provider.delete('user:{id}'.format(id=id))

            logger.info('Cache cleared successfully')
        except Exception:
            if retries > 5:
                logger.exception('Failed to run cleaning')
            else:
                retries += 1
                seconds = retries * retries
                logger.info('Failed to run cleaning. Retrying in {seconds} seconds'.format(seconds=seconds))
                time.sleep(seconds)
                self.clear_cache(ids, retries)

    def notify(self, notification_tokens, retries=0):
        try:
            logger.info('Notifying')

            for token in notification_tokens:
                fcmNotifier.notify(
                    registration_id=token,
                    title='Parabéns assinante! 🎉',
                    body='Você acaba de receber 50 corações ❤'
                )

            logger.info('Notifications successfully')
        except Exception:
            if retries > 5:
                logger.exception('Failed to send notifications')
            else:
                retries += 1
                seconds = retries * retries
                logger.info('Failed to send notifications. Retrying in {seconds} seconds'.format(seconds=seconds))
                time.sleep(seconds)
                self.notify(notification_tokens, retries)

    def run(self):
        logger.info('Running tasklog worker')
        logger.info('Requesting scroll')

        response = requests.post(
            search_url + '?scroll=5m',
            json.dumps(query),
            headers={'Content-Type': 'Application/json'},
            auth=HTTPBasicAuth(user, password)
        )

        response_json = response.json()
        hits_count = len(response_json['hits']['hits'])

        logger.info('{count} docs found.'.format(count=hits_count))

        while response.ok and hits_count > 0:
            docs = [{
                'update': {
                    '_id': hit['_id'],
                    '_type': hit['_type'],
                    '_index': hit['_index'],
                    'retry_on_conflict': 3
                }
            } for hit in response_json['hits']['hits']]

            ids = [hit['_id'] for hit in response_json['hits']['hits']]
            notification_tokens = [hit['_source']['fcmToken'] for hit in response_json['hits']['hits']]

            self.execude_bulk_update(docs)
            self.clear_cache(ids)
            self.notify(notification_tokens)

            logger.info('Requesting next scroll page')

            scroll_id = response_json['_scroll_id']

            scroll_url = '{es_host}/_search/scroll?scroll=5m&scroll_id={scroll_id}'.format(
                es_host=es_host,
                scroll_id=scroll_id
            )
            response = requests.get(
                scroll_url,
                auth=HTTPBasicAuth(user, password),
                headers={'Content-Type': 'Application/json'}
            )

            response_json = response.json()
            hits_count = len(response_json['hits']['hits'])

            logger.info('{count} docs found.'.format(count=hits_count))

        if not response.ok:
            logger.error(
                'Error scrolling index (http {status_code}): {error}'.format(
                    error=response.content,
                    status_code=response.status_code
                )
            )

        logger.info('Tasklog worker done')

    def execude_bulk_update(self, docs, retries=0):
        try:
            logger.info('Making bulk update')

            bulk_body = ''

            for doc in docs:
                bulk_body += json.dumps(doc) + "\n"
                bulk_body += json.dumps({
                    "script": {
                        "source": "ctx._source.available_hearts += params.available_hearts_count;",
                        "lang": "painless",
                        "params": {"available_hearts_count": 50}
                    }
                }) + "\n"

            bulk_url = '{es_host}/_bulk'.format(es_host=es_host)

            requests.post(
                bulk_url,
                bulk_body,
                auth=HTTPBasicAuth(user, password),
                headers={'Content-Type': 'Application/x-ndjson'}
            )

            logger.info('Bulk executed successfully')
        except Exception:
            if retries > 5:
                logger.exception('Failed to run bulk update')
            else:
                retries += 1
                seconds = retries * retries
                logger.info('Failed to run bulk update. Retrying in {seconds} seconds'.format(seconds=seconds))
                time.sleep(seconds)
                self.execude_bulk_update(docs, retries)


if __name__ == '__main__':
    user_hearts_worker = UserHeartsJob()
    schedule.every().monday.at('03:00').do(user_hearts_worker.run)

    logger.info('Worker running every moday at 03:00 AM')

    while True:
        schedule.run_pending()
        time.sleep(1)
