import os
import sys
import logging

level = int(os.environ.get('ORRTA_API_LOG_LEVEL', 10))
logging.basicConfig(stream=sys.stdout, level=level)
