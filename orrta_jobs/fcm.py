from pyfcm import FCMNotification
import logging

firebase_api_key = 'AAAAHaZAnr8:APA91bEGMt4pvQkn6JVHFzOcSHy1exkvojaijJEy64_kCcju-eSA-hPKsWSvSjyKj5EJsTrpL_53emIvLh0aL-_IaefrRLzZA2yxC59V0DoPRDlG3Zizf0jKLwcPyZlBRUvO1-tqEMDm'  # noqa


class FcmNotifier(object):

    def __init__(self):
        self._logger = logging.getLogger('FcmNotifier')
        self._push_service = FCMNotification(api_key=firebase_api_key)

    def notify(self, registration_id, title, body):
        try:
            self._logger.info('Notifying {id}: title: {title}; body: {body}'.format(
                id=registration_id,
                title=title,
                body=body
            ))

            self._push_service.notify_single_device(
                registration_id=registration_id,
                message_title=title,
                message_body=body
            )

            self._logger.info('Success to notify {id}: title: {title}; body: {body}'.format(
                id=registration_id,
                title=title,
                body=body
            ))
        except Exception:
            self._logger.exception('Failed to notify {id}: title: {title}; body: {body}'.format(
                id=registration_id,
                title=title,
                body=body
            ))
