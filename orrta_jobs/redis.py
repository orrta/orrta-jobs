import logging
import redis

redis_url = 'redis://h:p5777486eba9f14fadf5a598c1dfa367f7bc1b51f59431977a2ae165ca81ea7e8@ec2-34-203-121-173.compute-1.amazonaws.com:7139'  # noqa

logger = logging.getLogger('redis')
redis_connection = redis.Redis.from_url(redis_url)


class CacheProvider:

    def delete(self, key):
        try:
            redis_connection.delete(key)
        except Exception:
            logger.exception('failed to delete cache')

    def keys(self, pattern):
        try:
            cache_keys = redis_connection.keys(pattern)
            return cache_keys or []
        except Exception:
            logger.exception('failed to get cache keys')
            return []
